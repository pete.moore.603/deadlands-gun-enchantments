import {
  moduleName,
  MATCH_ANY,
  PATH,
  FLAG,
  WEAPON,
  ANIMATION,
  BLOOD,
  debug,
} from './const.js';
import {
  DEADLANDS_BLOOD,
} from './init.js';
import {
  checkForCommas,
} from './configuration/settings.js';

class Blood {
  constructor(setting, file) {
    debug('blood constructor: setting ' + setting);
    let defaultType = (setting == BLOOD.RED.setting);

    this.name = setting.toLowerCase().replace('blood','');
    this.settingName = setting;
    this.setting = game.settings.get(moduleName, setting);
    // Trim the weapon list entries.
    this.list = this.setting.split('|').map(el => el.trim());
    // Build a regex for later use; escape certain special characters
    // for non-default types.  The default type matches any creature.
    let regexStr = defaultType ? MATCH_ANY : this.list.join('|').replace(/[-[/\]{}()*+?.,\\^$#]/g, '\\$&');
    this.regex = new RegExp('.?(' + regexStr + ')');
    this.file = file;
    // Bypass config page for the default blood type (red).
    this.bypassConfig = defaultType;
  }
}

export function initBlood() {
  let dir = PATH.VIDEO;
  let blood = DEADLANDS_BLOOD;

  let i = BLOOD.BLACK.setting;
  blood[i] = new Blood(i, dir + 'bloodSplatBlack.webm');

  i = BLOOD.BLUE.setting;
  blood[i] = new Blood(i, dir + 'bloodSplatBlue.webm');

  i = BLOOD.GREEN.setting;
  blood[i] = new Blood(i, dir + 'bloodSplatGreen.webm');

  i = BLOOD.RED.setting;
  blood[i] = new Blood(i, [dir + 'bloodSplatRed1.webm', dir + 'bloodSplatRed2.webm',
                      dir + 'bloodSplatRed3.webm', dir + 'bloodSplatRed4.webm',
                      dir + 'bloodSplatRed5.webm']);

  // Check for comma delimiters.
  checkForCommas(blood);
}

// Find the weapon based on the creature name.
export function getBlood(name) {
  let blood = null;

  for (let key in DEADLANDS_BLOOD) { 
    let checkBlood = DEADLANDS_BLOOD[key];
    let pos = name.search(checkBlood.regex);
    if (pos != -1) {
      blood = checkBlood;
      break;
    }
  };

  return blood ? blood : null;
}

